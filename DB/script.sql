create database Bank
use bank

   create table cliente (
       id int identity not null,
       nombres varchar(50) not null,
		apellidos varchar(50) not null,
       cui int not null,
		nit int,
		telefono int not null,
		direccion varchar(100) not null,
		email varchar(50),
       primary key (id)
   );
	create table tipo_cuenta(
		id int identity not null,
		cuenta varchar(50),
		primary key(id)
	);
	create table divisa(
		id int identity not null,
		divisa varchar(50),
		primary key(id)
	);
	create table cuenta(
		id int identity not null,
		id_cliente int,
		numero int,
		id_tipo int,
		id_divisa int,
		primary key(id),
		foreign key(id_cliente) references cliente(id),
		foreign key(id_tipo) references tipo_cuenta(id),
		foreign key(id_divisa) references divisa(id)
	);
	create table estado_tarjeta(
		id int identity not null,
		estado varchar(50),
		primary key(id)
	);
	create table tarjeta(
		id int identity not null,
		id_cuenta int,
		id_estado int,
		numero int,
		nombre varchar,
		fecha_vencimiento varchar(5),
		cvs int,
		primary key(id),
		foreign key(id_estado) references estado_tarjeta(id),
		foreign key(id_cuenta) references cuenta(id)
	);


 
