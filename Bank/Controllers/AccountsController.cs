﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Bank;

namespace Bank.Controllers
{
    public class AccountsController : Controller
    {
        private BankEntities db = new BankEntities();

        // GET: Accounts
        public ActionResult Index()
        {
            var cuenta = db.cuenta.Include(c => c.cliente).Include(c => c.divisa).Include(c => c.tipo_cuenta);
            return View(cuenta.ToList());
        }

        // GET: Accounts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cuenta cuenta = db.cuenta.Find(id);
            if (cuenta == null)
            {
                return HttpNotFound();
            }
            return View(cuenta);
        }

        // GET: Accounts/Create
        public ActionResult Create()
        {
            ViewBag.id_cliente = new SelectList(db.cliente, "id", "nombres");
            ViewBag.id_divisa = new SelectList(db.divisa, "id", "divisa1");
            ViewBag.id_tipo = new SelectList(db.tipo_cuenta, "id", "cuenta");
            return View();
        }

        // POST: Accounts/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,id_cliente,numero,id_tipo,id_divisa")] cuenta cuenta)
        {
            if (ModelState.IsValid)
            {
                db.cuenta.Add(cuenta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_cliente = new SelectList(db.cliente, "id", "nombres", cuenta.id_cliente);
            ViewBag.id_divisa = new SelectList(db.divisa, "id", "divisa1", cuenta.id_divisa);
            ViewBag.id_tipo = new SelectList(db.tipo_cuenta, "id", "cuenta", cuenta.id_tipo);
            return View(cuenta);
        }

        // GET: Accounts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cuenta cuenta = db.cuenta.Find(id);
            if (cuenta == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_cliente = new SelectList(db.cliente, "id", "nombres", cuenta.id_cliente);
            ViewBag.id_divisa = new SelectList(db.divisa, "id", "divisa1", cuenta.id_divisa);
            ViewBag.id_tipo = new SelectList(db.tipo_cuenta, "id", "cuenta", cuenta.id_tipo);
            return View(cuenta);
        }

        // POST: Accounts/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,id_cliente,numero,id_tipo,id_divisa")] cuenta cuenta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cuenta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_cliente = new SelectList(db.cliente, "id", "nombres", cuenta.id_cliente);
            ViewBag.id_divisa = new SelectList(db.divisa, "id", "divisa1", cuenta.id_divisa);
            ViewBag.id_tipo = new SelectList(db.tipo_cuenta, "id", "cuenta", cuenta.id_tipo);
            return View(cuenta);
        }

        // GET: Accounts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cuenta cuenta = db.cuenta.Find(id);
            if (cuenta == null)
            {
                return HttpNotFound();
            }
            return View(cuenta);
        }

        // POST: Accounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            cuenta cuenta = db.cuenta.Find(id);
            db.cuenta.Remove(cuenta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
