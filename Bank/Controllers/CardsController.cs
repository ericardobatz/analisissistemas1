﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Bank;

namespace Bank.Controllers
{
    public class CardsController : Controller
    {
        private BankEntities db = new BankEntities();

        // GET: Cards
        public ActionResult Index()
        {
            var tarjeta = db.tarjeta.Include(t => t.cuenta).Include(t => t.estado_tarjeta);
            return View(tarjeta.ToList());
        }

        // GET: Cards/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tarjeta tarjeta = db.tarjeta.Find(id);
            if (tarjeta == null)
            {
                return HttpNotFound();
            }
            return View(tarjeta);
        }

        // GET: Cards/Create
        public ActionResult Create()
        {
            ViewBag.id_cuenta = new SelectList(db.cuenta, "id", "id");
            ViewBag.id_estado = new SelectList(db.estado_tarjeta, "id", "estado");
            return View();
        }

        // POST: Cards/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,id_cuenta,id_estado,numero,nombre,fecha_vencimiento,cvs")] tarjeta tarjeta)
        {
            if (ModelState.IsValid)
            {
                db.tarjeta.Add(tarjeta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_cuenta = new SelectList(db.cuenta, "id", "id", tarjeta.id_cuenta);
            ViewBag.id_estado = new SelectList(db.estado_tarjeta, "id", "estado", tarjeta.id_estado);
            return View(tarjeta);
        }

        // GET: Cards/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tarjeta tarjeta = db.tarjeta.Find(id);
            if (tarjeta == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_cuenta = new SelectList(db.cuenta, "id", "id", tarjeta.id_cuenta);
            ViewBag.id_estado = new SelectList(db.estado_tarjeta, "id", "estado", tarjeta.id_estado);
            return View(tarjeta);
        }

        // POST: Cards/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,id_cuenta,id_estado,numero,nombre,fecha_vencimiento,cvs")] tarjeta tarjeta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tarjeta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_cuenta = new SelectList(db.cuenta, "id", "id", tarjeta.id_cuenta);
            ViewBag.id_estado = new SelectList(db.estado_tarjeta, "id", "estado", tarjeta.id_estado);
            return View(tarjeta);
        }

        // GET: Cards/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tarjeta tarjeta = db.tarjeta.Find(id);
            if (tarjeta == null)
            {
                return HttpNotFound();
            }
            return View(tarjeta);
        }

        // POST: Cards/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tarjeta tarjeta = db.tarjeta.Find(id);
            db.tarjeta.Remove(tarjeta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
